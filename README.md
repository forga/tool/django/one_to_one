`one-to-one`
============

[ [🇫🇷 French version](README-fr.md) ]

This [Python package](https://docs.python.org/en/3/tutorial/modules.html#packages) is part of the [tools for Django](https://gitlab.com/forga/tool/django/) in group [`forga`](https://gitlab.com/forga/). For more information about this group, visit this URL :

https://forga.gitlab.io/en/


💡 What for  ?
-------------

1. use [`modelformset_factory`](https://docs.djangoproject.com/en/3.1/ref/forms/models/#modelformset-factory) to propose a **unique form** to add an entry **simultaneously in the 2 models** (`Foo` & `Bar`);
1. try the [packaging for Django](https://docs.djangoproject.com/en/3.1/intro/reusable-apps/);
1. distribute the code to **install it with `pip`**, but without going through [PyPI](https://pypi.org/) [[source](https://stackoverflow.com/a/35998253/6709630)];
1. **develop** independently of a final project;


🔧 How to do it ?
----------------

### Installation in an existing project

1. Add this repository to `requirements.txt` and install it (in a virtual environment) :

```shell
echo git+https://gitlab.com/forga/tool/django/one_to_one.git@stable#egg=one_to_one >> requirements.txt
pip install -r requirements.txt
```

2. Add the app to `settings.INSTALLED_APPS` :

```shell
INSTALLED_APPS = [
    …
    "one_to_one",
]
```

3. Add URLs configuration to `urls.urlpatterns` :

```shell
urlpatterns = [
    …
    path("one_to_one/", include("one_to_one.urls")),
]
```

4. _[option]_ Add a link in your _template(s)_ :

```shell
<a href="{% url 'one_to_one:list-foo' %}">ONE_TO_ONE list</a>
```

5. _[option]_ In order for your _GitLab runner_ to retrieve the code to be installed from its repository, add the `git` installation to your `.gitlab-ci.yml` :

```shell
    script:
    - apt-get update && apt-get -y install git
    …
```


### Local development

1. Clone it, install the dependencies in a virtual environment :

```shell
git clone git@gitlab.com:forga/tool/django/one_to_one.git forga-one_to_one
cd forga-one_to_one
path/to/python3.7 -m venv ~/.venvs/forga-one_to_one
source ~/.venvs/forga-one_to_one/bin/activate
pip install -r requirements.txt
pip install -r requirements-dev.txt
```

2. Let's add code :

```shell
$EDITOR one_to_one/tests.py
```


📦 What's in it  ?
-----------------

* [2 data models](https://gitlab.com/forga/tool/django/one_to_one/-/blob/stable/one_to_one/models.py) with a _one-for-one_ relationship (`Foo` & `Bar`);
* [1 form](https://gitlab.com/forga/tool/django/one_to_one/-/blob/stable/one_to_one/views.py#L21) allowing the simultaneous input of the 2 models;
* the [minimum configuration](https://gitlab.com/forga/tool/django/one_to_one/-/commit/8c3414dd60af32cc143d55cdbd39635ff0a0b974) to [install this package with `pip` in another project](https://gitlab.com/forga/customer/acme/-/commit/aa7bf17f9b8e1ff1d75d9c289c2bd2b017689f64);


🤝 Want to contribute  ?
-----------------------

With pleasure!

There are some conventions ([git](https://forga.gitlab.io/process/fr/manuel/convention/git/) & [python](https://forga.gitlab.io/process/fr/manuel/convention/python/)) presented in [the manual](https://forga.gitlab.io/process/fr/manuel/) and a [code of conduct](https://forga.gitlab.io/process/fr/manuel/human/code-of-conduct/) (in French).

And why not propose an [edition of this page](https://gitlab.com/forga/tool/django/one_to_one/-/edit/documentation/README.md) ?


🐘 For the record
-----------------

The code was originally hosted on the [`free_zed/djbp`](https://gitlab.com/free_zed/djbp) repository, but the organization didn't really allow to stay [DRY](https://en.wikipedia.org/wiki/Don%27t_repeat_yourself).
