from django.db import models
from django.utils import timezone


class Foo(models.Model):
    name = models.CharField("name", max_length=30)
    text = models.TextField("text")
    date_created = models.DateTimeField("date created", default=timezone.now)
    date_edit = models.DateTimeField("last edit", auto_now=True, blank=True, null=True)

    def __str__(self):
        """ Return shortname """
        return self.get_name()

    def get_name(self):
        """Return the name"""
        return self.name

    def all_attr(self):
        """ return a dict containing all attributes """
        return {
            attr: val for (attr, val) in vars(self).items() if not attr.startswith("_")
        }


class Bar(models.Model):
    foo = models.OneToOneField(Foo, on_delete=models.CASCADE)
    name = models.CharField("name", max_length=30, blank=True)
    text = models.TextField("text", blank=True)
    date_created = models.DateTimeField("date created", default=timezone.now)
    date_edit = models.DateTimeField("last edit", auto_now=True, blank=True, null=True)

    def __str__(self):
        """ Return shortname """
        return self.get_name()

    def get_name(self):
        """Return the name"""
        return self.name

    def all_attr(self):
        """ return a dict containing all attributes """
        return {
            attr: val for (attr, val) in vars(self).items() if not attr.startswith("_")
        }
