from pytest import fixture, mark

from django.urls import reverse
from django.contrib.messages import get_messages

from one_to_one.models import Bar, Foo


FIXTURE_FOO = [
    (1, "first foo name", "first foo text"),
    (2, "second foo name", "second foo text"),
]

FIXTURE_BAR = [
    (1, 2, "first bar name", "first bar text"),
    (2, 1, "second bar name", "second bar text"),
]

TEMPLATES_ADD = {
    "one_to_one/one_to_one_form.dhtml": 1,
    "one_to_one/base.dhtml": 1,
    "django/forms/widgets/attrs.html": 4,
    "django/forms/widgets/input.html": 2,
    "django/forms/widgets/textarea.html": 2,
    "django/forms/widgets/text.html": 2,
}

TEMPLATES_LIST = {
    "one_to_one/one_to_one_list.dhtml": 1,
    "one_to_one/base.dhtml": 1,
}

# #############################################################################
# ##     Fixtures
# #############################################################################
@fixture
def new_one_to_one_add():
    return [
        {"foo-name": "AAA", "foo-text": "BBB", "bar-name": "CCC", "bar-text": "DDD"},
        {"foo-name": "bbb", "foo-text": "ccc", "bar-name": "ddd", "bar-text": "eee"},
        {"foo-name": "EEE", "foo-text": "FFF", "bar-name": "GGG", "bar-text": "HHH"},
    ]


# #############################################################################
#   one_to_one.urls
# #############################################################################
@mark.parametrize(
    "url, status, templates",
    [
        (reverse("one_to_one:list-foo"), 200, TEMPLATES_LIST,),
        (reverse("one_to_one:list-bar"), 200, TEMPLATES_LIST,),
        (reverse("one_to_one:add"), 200, TEMPLATES_ADD,),
    ],
)
@mark.django_db
def test_route_anon(client_anon, url, status, templates):
    """ Granted URL access for visitor (not logged in)"""
    response = client_anon.get(url)
    current_template_list = [t.name for t in response.templates]

    assert response.status_code == status
    for template in templates:
        assert template in current_template_list


# #############################################################################
#   one_to_one.views
# #############################################################################
@mark.parametrize(
    "posted_data, redir_chain, message, tag, templates",
    [
        (
            {
                "foo-name": "AAA",
                "bar-name": "CCC",
                "foo-text": "BBB",
                "bar-text": "DDD",
            },
            [(reverse("one_to_one:list-foo"), 302)],
            "Data saved",
            "success",
            TEMPLATES_LIST,
        ),
        (
            {"field": "something", "other_field": "something else"},
            [],
            "Error in form",
            "warning",
            TEMPLATES_ADD,
        ),
    ],
)
@mark.django_db
def test_route_one_to_one_form_post(
    client_anon, posted_data, redir_chain, message, tag, templates
):
    """ Posting some Foo & Bar form data """

    response = client_anon.post(reverse("one_to_one:add"), posted_data, follow=True)

    message_list = [msg for msg in get_messages(response.wsgi_request)]
    current_template_list = [t.name for t in response.templates]

    assert message_list[0].message == message
    assert response.status_code == 200
    assert response.redirect_chain == redir_chain

    for template in templates:
        assert template in current_template_list


@mark.django_db
def test_route_one_to_one_create_post(client_anon, new_one_to_one_add):
    """ Create a new one_to_one """

    response = client_anon.post(reverse("one_to_one:add"), new_one_to_one_add[2])
    last_bar = Bar.objects.last()

    assert response.status_code == 302
    assert response.url == reverse("one_to_one:list-foo")
    assert last_bar.name == new_one_to_one_add[2]["bar-name"]


# #############################################################################
#   one_to_one.models
# #############################################################################
@mark.parametrize("f_id, name, text", FIXTURE_FOO)
@mark.django_db
def test_foo__str__(client_anon, f_id, name, text):
    foo = Foo.objects.get(text=text)

    assert foo.__str__() == name


@mark.parametrize("f_id, name, text", FIXTURE_FOO)
@mark.django_db
def test_foo_all_attr(client_anon, f_id, name, text):
    foo = Foo.objects.get(id=f_id)
    foo_dict = foo.all_attr()

    assert foo_dict["name"] == name
    assert foo_dict["text"] == text


@mark.parametrize("f_id, name, text", FIXTURE_FOO)
@mark.django_db
def test_foo_get_name(client_anon, f_id, name, text):
    foo = Foo.objects.get(text=text)

    assert foo.get_name() == name


@mark.parametrize("b_id, f_id, name, text", FIXTURE_BAR)
@mark.django_db
def test_bar__str__(client_anon, b_id, f_id, name, text):
    bar = Bar.objects.get(text=text)

    assert bar.__str__() == name


@mark.parametrize("b_id, f_id, name, text", FIXTURE_BAR)
@mark.django_db
def test_bar_all_attr(client_anon, b_id, f_id, name, text):
    bar = Bar.objects.get(id=b_id)
    bar_dict = bar.all_attr()

    assert bar_dict["name"] == name
    assert bar_dict["text"] == text


@mark.parametrize("b_id, f_id, name, text", FIXTURE_BAR)
@mark.django_db
def test_bar_get_name(client_anon, b_id, f_id, name, text):
    bar = Bar.objects.get(text=text)

    assert bar.get_name() == name
