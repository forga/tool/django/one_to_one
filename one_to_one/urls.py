from django.urls import path
from one_to_one.views import FooList, BarList, one_to_one_create

app_name = "one_to_one"
urlpatterns = [
    path("", FooList.as_view(), name="list-foo"),
    path("add", one_to_one_create, name="add"),
    path("bar/", BarList.as_view(), name="list-bar"),
]
