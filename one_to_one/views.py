from django.contrib import messages
from django.forms import modelform_factory
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse
from django.views.generic import ListView

from one_to_one.models import Bar, Foo


class FooList(ListView):
    model = Foo
    template_name = "one_to_one/one_to_one_list.dhtml"


class BarList(ListView):
    model = Bar
    template_name = "one_to_one/one_to_one_list.dhtml"


def one_to_one_create(request):
    FooModelForm = modelform_factory(Foo, fields=("name", "text"))
    BarModelForm = modelform_factory(Bar, fields=("name", "text"))

    if request.method == "POST":
        foo_formset = FooModelForm(request.POST, prefix="foo")
        bar_formset = BarModelForm(request.POST, prefix="bar")

        if foo_formset.is_valid() and bar_formset.is_valid():
            new_foo = foo_formset.save()
            new_bar = bar_formset.save(commit=False)
            new_bar.foo = new_foo
            new_bar.save()
            messages.success(request, "Data saved")

            return HttpResponseRedirect(reverse("one_to_one:list-foo"))

        else:
            messages.warning(request, "Error in form")
    else:
        foo_formset = FooModelForm(prefix="foo")
        bar_formset = BarModelForm(prefix="bar")

    return render(
        request,
        "one_to_one/one_to_one_form.dhtml",
        {"foo_formset": foo_formset, "bar_formset": bar_formset},
    )
