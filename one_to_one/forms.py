from django.forms import ModelForm

from one_to_one.models import Foo, Bar


class FooForm(ModelForm):
    class Meta:
        model = Foo
        fields = ["name", "text"]


class BarForm(ModelForm):
    class Meta:
        model = Bar
        fields = ["name", "text"]
