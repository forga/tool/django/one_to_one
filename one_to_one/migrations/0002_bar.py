# Generated by Django 2.2.3 on 2019-07-26 12:43

from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [("one_to_one", "0001_initial")]

    operations = [
        migrations.CreateModel(
            name="Bar",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "bar_name",
                    models.CharField(
                        blank=True, max_length=30, verbose_name="bar_name"
                    ),
                ),
                ("text", models.TextField(blank=True, verbose_name="text")),
                (
                    "date_created",
                    models.DateTimeField(
                        default=django.utils.timezone.now, verbose_name="date created"
                    ),
                ),
                (
                    "date_edit",
                    models.DateTimeField(
                        auto_now=True, null=True, verbose_name="last edit"
                    ),
                ),
                (
                    "foo",
                    models.OneToOneField(
                        on_delete=django.db.models.deletion.CASCADE, to="one_to_one.Foo"
                    ),
                ),
            ],
        )
    ]
