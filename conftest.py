from pytest import fixture

from django.core.management import call_command
from django.test import Client


# #############################################################################
# ##     Fixtures
# #############################################################################
@fixture
# pylint: disable=W0621,W0613
def django_db_setup(django_db_setup, django_db_blocker):
    with django_db_blocker.unblock():
        call_command("loaddata", "foo-tests.json")

    with django_db_blocker.unblock():
        call_command("loaddata", "bar-tests.json")


@fixture
def client_anon():
    """ Provide a test client with anonymous user (not authenticated) """
    return Client()
