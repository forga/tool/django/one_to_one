`one-to-one`
============

Ce [paquet Python](https://docs.python.org/fr/3/tutorial/modules.html#packages) fait parti des [outils pour Django](https://gitlab.com/forga/tool/django/) du groupe [`forga`](https://gitlab.com/forga/). Pour plus d'informations sur ce groupe, visitez l'URL :

https://forga.gitlab.io/fr/


💡 Pourquoi faire ?
------------------

1. utiliser [`modelformset_factory`](https://docs.djangoproject.com/fr/3.1/ref/forms/models/#modelformset-factory) pour proposer un **formulaire unique** pour ajouter une entrée **simultanément dans les 2 modèles** (`Foo` & `Bar`);
1. essayer l'[empaquetage pour Django](https://docs.djangoproject.com/fr/3.1/intro/reusable-apps/);
1. distribuer le code pour **l'installer avec `pip`**, mais sans passer par [PyPI](https://pypi.org/) [[source](https://stackoverflow.com/a/35998253/6709630)];
1. **développer** indépendamment d'un projet final;


🔧 Comment faire ?
-----------------

### Installation dans un projet existant

1. Ajouter ce dépôt dans `requirements.txt` et l'installer (dans un environnement virtuel) :

```shell
echo git+https://gitlab.com/forga/tool/django/one_to_one.git@stable#egg=one_to_one >> requirements.txt
pip install -r requirements.txt
```

2. Ajouter l'app. dans `settings.INSTALLED_APPS` :

```shell
INSTALLED_APPS = [
    …
    "one_to_one",
]
```

3. Ajouter la configuration d'URLs dans `urls.urlpatterns` :

```shell
urlpatterns = [
    …
    path("one_to_one/", include("one_to_one.urls")),
]
```

4. _[option]_ Ajouter un lien dans un/vos _template(s)_ :

```shell
<a href="{% url 'one_to_one:list-foo' %}">ONE_TO_ONE list</a>
```

5. _[option]_ Pour que votre _runner GitLab_ puisse récupérer le code à installer depuis son dépôt, ajouter l'installation de `git` dans votre `.gitlab-ci.yml` :

```shell
    script:
    - apt-get update && apt-get -y install git
    …
```


### Développement en local

1. Clonez-le, installez les dépendances dans un environnement virtuel :

```shell
git clone git@gitlab.com:forga/tool/django/one_to_one.git forga-one_to_one
cd forga-one_to_one
path/to/python3.7 -m venv ~/.venvs/forga-one_to_one
source ~/.venvs/forga-one_to_one/bin/activate
pip install -r requirements.txt
pip install -r requirements-dev.txt
```

2. On peut maintenant ajouter du code :

```shell
$EDITOR one_to_one/tests.py
```

📦 Qu'est ce qu'il y a dedans ?
-------------------------------

* [2 modèles de donnée](https://gitlab.com/forga/tool/django/one_to_one/-/blob/stable/one_to_one/models.py) avec une relation _un-pour-un_ (`Foo` & `Bar`);
* [1 formulaire](https://gitlab.com/forga/tool/django/one_to_one/-/blob/stable/one_to_one/views.py#L21) permettant la saisie simultanée des 2 modèles;
* la [configuration minimum](https://gitlab.com/forga/tool/django/one_to_one/-/commit/8c3414dd60af32cc143d55cdbd39635ff0a0b974) pour [installer ce paquet avec `pip` dans un autre projet](https://gitlab.com/forga/customer/acme/-/commit/aa7bf17f9b8e1ff1d75d9c289c2bd2b017689f64);


🤝 Envie de contribuer ?
------------------------

Avec plaisir !

Il y a quelques conventions ([git](https://forga.gitlab.io/process/fr/manuel/convention/git/) & [python](https://forga.gitlab.io/process/fr/manuel/convention/python/)) présentées dans [le manuel](https://forga.gitlab.io/process/fr/manuel/) ainsi qu'un [code de conduite](https://forga.gitlab.io/process/fr/manuel/human/code-of-conduct/).

Et pourquoi ne pas proposer une [édition de cette page](https://gitlab.com/forga/tool/django/one_to_one/-/edit/documentation/README-fr.md)?


🐘 Pour mémoire
---------------

Le code était originellement hébergé sur le dépôt [`free_zed/djbp`](https://gitlab.com/free_zed/djbp), mais l'organisation ne permettait pas vraiment de rester [DRY](https://fr.wikipedia.org/wiki/Ne_vous_r%C3%A9p%C3%A9tez_pas).
